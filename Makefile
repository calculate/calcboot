## -----------------------------------------------------------------------
##
##   Copyright 2001-2008 H. Peter Anvin - All Rights Reserved
##
##   This program is free software; you can redistribute it and/or modify
##   it under the terms of the GNU General Public License as published by
##   the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
##   Boston MA 02110-1301, USA; either version 2 of the License, or
##   (at your option) any later version; incorporated herein by reference.
##
## -----------------------------------------------------------------------
## -----------------------------------------------------------------------
##
##   02.06.2009 Calculate Pack http://www.calculate-linux.org
##   Добавлена компиляция calcmenu.c32
##
## -----------------------------------------------------------------------

##
## Simple menu system
##

topdir = ../..
include ../MCONFIG

LIBS	   = ../libutil/libutil_com.a ../lib/libcom32.a $(LIBGCC)
LNXLIBS	   = ../libutil/libutil_lnx.a

MODULES	  = menu.c32 vesamenu.c32 calcmenu.c32
TESTFILES =

COMMONOBJS = passwd.o drain.o printmsg.o colors.o \
	background.o refstr.o execute.o 

CALCMENU = calcmenumain.o calcreadconfig.o

COMMONMENU = menumain.o readconfig.o

all: $(MODULES) $(TESTFILES)

menu.elf : menu.o $(COMMONOBJS) $(COMMONMENU) $(LIBS) $(C_LIBS)
	$(LD) $(LDFLAGS) -o $@ $^

calcmenu.elf : calcmenu.o $(COMMONOBJS) $(CALCMENU) $(LIBS) $(C_LIBS)
	$(LD) $(LDFLAGS) -o $@ $^

vesamenu.elf : vesamenu.o $(COMMONOBJS) $(COMMONMENU) $(LIBS) $(C_LIBS)
	$(LD) $(LDFLAGS) -o $@ $^

tidy dist:
	rm -f *.o *.lo *.a *.lst *.elf .*.d *.tmp

clean: tidy
	rm -f *.lnx

spotless: clean
	rm -f *.lss *.c32 *.com
	rm -f *~ \#*

install:

-include .*.d
